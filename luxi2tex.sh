echo "******************************************************"
echo "  Vytvářím metrické soubory (.tfm) fontů Luxi"
echo "  od Bigelowa a Holmese z roku 2001"
echo "******************************************************"

./ttf4tex.sh luxi luxirr  flxrr    IL2-WGL4 3 1
./ttf4tex.sh luxi luxirri flxrti   IL2-WGL4 3 1 
./ttf4tex.sh luxi luxirb  flxrbx   IL2-WGL4 3 1 
./ttf4tex.sh luxi luxirbi flxrbxti IL2-WGL4 3 1 

./ttf4tex.sh luxi luximr  flxmr    IL2-WGL4 3 1 
./ttf4tex.sh luxi luximri flxmti   IL2-WGL4 3 1 
./ttf4tex.sh luxi luximb  flxmbx   IL2-WGL4 3 1 
./ttf4tex.sh luxi luximbi flxmbxti IL2-WGL4 3 1 

./ttf4tex.sh luxi luxisr  flxsr    IL2-WGL4 3 1 
./ttf4tex.sh luxi luxisri flxsti   IL2-WGL4 3 1 
./ttf4tex.sh luxi luxisb  flxsbx   IL2-WGL4 3 1 
./ttf4tex.sh luxi luxisbi flxsbxti IL2-WGL4 3 1

#mktexlsr /usr/TeX/texmf-local
#mktexlsr /usr/TeX/texmf
#mktexlsr /usr/TeX/texmf-var
mktexlsr

