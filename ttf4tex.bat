@echo off
REM Syntax: ttf4tex <rodina> <jm�no ttf> <jm�no tfm a vf> <k�dov�n� enc> <platforma> <k�dov�n� ttf>
@echo on

REM set tfmadr=D:\TeX\texmf\fonts\tfm\public\%1
set tfmadr=C:\Program Files\TeXLive\2008\texmf-dist\fonts\tfm\public\%1

REM set vfadr=D:\TeX\texmf\fonts\vf\public\%1
set vfadr=C:\Program Files\TeXLive\2008\texmf-dist\fonts\vf\public\%1

if not exist "%tfmadr%" mkdir "%tfmadr%"
if not exist "%vfadr%"  mkdir "%vfadr%"

ttf2tfm %2.ttf -T %4 -P %5 -E %6 -v "%3" "%3"
@echo off
rem v*.vf - vf mus� m�t jin� z�kladn� jm�no, ne� tfm, a jin�, ne� ttf!
@echo on
vptovf "%3" "v%3" "%3" 

del "%3.vpl"

move /Y "v%3.vf" "%vfadr%" 
move /Y "%3.tfm" "%tfmadr%" 
