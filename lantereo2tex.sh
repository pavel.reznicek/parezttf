#/bin/bash

echo "***********************************************************"
echo "  Vytvářím metrické soubory (.tfm) pro font LantereoScript "
echo "***********************************************************"

./ttf4tex.sh lantereo lantereoscript  flanmn  IL2-WGL4 3 1 
./ttf4tex.sh lantereo lantereoscript  flanmit IL2-WGL4 3 1 
./ttf4tex.sh lantereo lantereoscript  flanbn  IL2-WGL4 3 1 
./ttf4tex.sh lantereo lantereoscript  flanbit IL2-WGL4 3 1 

#mktexlsr /usr/TeX/texmf-local
#mktexlsr /usr/TeX/texmf-var
#mktexlsr /usr/TeX/texmf
mktexlsr
