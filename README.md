# parezttf

Config files for ttf2pk and installation scripts for the TTF fonts used by the parez LaTeX package.

Some of the used TTF fonts are copyrighted so they can’t be included here.

Contents of this repository should go to the /ttf2pk directory of your texmf tree (do with caution;
at least the *.map files should be merged with yours; don't accidentally overwrite your own settings!).
