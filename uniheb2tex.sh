#/bin/bash

echo "******************************************************"
echo "  Vytvářím metrické soubory (.tfm) pro font Shebrew"
echo "******************************************************"

./ttf4tex.sh taamey TaameyFrankCLM-Medium  		 ftafr    IL2-UnicodeHebrew 3 1 
./ttf4tex.sh taamey TaameyFrankCLM-MediumOblique ftafti   IL2-UnicodeHebrew 3 1 
./ttf4tex.sh taamey TaameyFrankCLM-Bold			 ftafbx   IL2-UnicodeHebrew 3 1 
./ttf4tex.sh taamey TaameyFrankCLM-BoldOblique	 ftafbxti IL2-UnicodeHebrew 3 1 

./ttf4tex.sh taamey TaameyDavidCLM-Medium  		 ftadr    IL2-UnicodeHebrew 3 1 
./ttf4tex.sh taamey TaameyDavidCLM-MediumOblique ftadti   IL2-UnicodeHebrew 3 1 
./ttf4tex.sh taamey TaameyDavidCLM-Bold  		 ftadbx   IL2-UnicodeHebrew 3 1 
./ttf4tex.sh taamey TaameyDavidCLM-BoldOblique	 ftadbxti IL2-UnicodeHebrew 3 1 

./ttf4tex.sh dorian DorianCLM-Book  			 fdorr    IL2-UnicodeHebrew 3 1 
./ttf4tex.sh dorian DorianCLM-BookItalic 		 fdorti   IL2-UnicodeHebrew 3 1 
./ttf4tex.sh dorian DorianCLM-Book  			 fdorbx   IL2-UnicodeHebrew 3 1 
./ttf4tex.sh dorian DorianCLM-BookItalic 		 fdorbxti IL2-UnicodeHebrew 3 1 

#mktexlsr /usr/TeX/texmf-local
#mktexlsr /usr/TeX/texmf-var
#mktexlsr /usr/TeX/texmf
mktexlsr
