#/bin/bash

echo "******************************************************"
echo "  Vytvářím metrické soubory (.tfm) pro font Shebrew"
echo "******************************************************"

./ttf4tex.sh shebrew shebrew  fhebr    shebrew 3 0
./ttf4tex.sh shebrew shebrew  fhebti   shebrew 3 0 
./ttf4tex.sh shebrew shebrew  fhebbx   shebrew 3 0 
./ttf4tex.sh shebrew shebrew  fhebbxti shebrew 3 0 

#mktexlsr /usr/TeX/texmf-local
#mktexlsr /usr/TeX/texmf-var
#mktexlsr /usr/TeX/texmf
mktexlsr
