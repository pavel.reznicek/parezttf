@echo ******************************************************
@echo   Vytv���m metrick� soubory (.tfm) font� Luxi
@echo   od Bigelowa a Holmese z roku 2001
@echo ******************************************************

@echo off
call ttf4tex luxi luxirr  flxrr    IL2-WGL4 3 1
call ttf4tex luxi luxirri flxrti   IL2-WGL4 3 1 
call ttf4tex luxi luxirb  flxrbx   IL2-WGL4 3 1 
call ttf4tex luxi luxirbi flxrbxti IL2-WGL4 3 1 

call ttf4tex luxi luximr  flxmr    IL2-WGL4 3 1
call ttf4tex luxi luximri flxmti   IL2-WGL4 3 1
call ttf4tex luxi luximb  flxmbx   IL2-WGL4 3 1
call ttf4tex luxi luximbi flxmbxti IL2-WGL4 3 1

call ttf4tex luxi luxisr  flxsr    IL2-WGL4 3 1
call ttf4tex luxi luxisri flxsti   IL2-WGL4 3 1
call ttf4tex luxi luxisb  flxsbx   IL2-WGL4 3 1
call ttf4tex luxi luxisbi flxsbxti IL2-WGL4 3 1

rem mktexlsr -u "%UserProfile%\Dokumenty\TeX\texmf"
rem mktexlsr -u"D:\Dokumenty\TeX\texmf"
rem mktexlsr "D:\TeX\texmf"
mktexlsr
