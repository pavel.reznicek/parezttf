#!/bin/bash
# Syntax: ttf4tex <rodina> <jméno ttf> <jméno tfm a vf> <kódování enc> <platforma> <kodovani ttf>
#                  1        2           3                4              5           6
#tfmadr=/usr/TeX/texmf-dist/fonts/tfm/public/$1
#tfmadr=/usr/share/texmf/fonts/tfm/public/$1
tfmadr=../fonts/tfm/public/$1

#vfadr=/usr/TeX/texmf-dist/fonts/vf/public/$1
#vfadr=/usr/share/texmf/fonts/vf/public/$1
vfadr=../fonts/tfm/public/$1

if ! [[ -a $tfmadr ]]; then mkdir -p $tfmadr;  fi
if ! [[ -a $vfadr ]];  then mkdir -p $vfadr;   fi

ttf2tfm $2.ttf -T $4 -P $5 -E $6 -v $3 $3
# v*.vf - vf musí mít jiné základní jméno, než tfm, a jiné, než ttf!
vptovf $3 v$3 $3

rm -f $3.vpl

mv -f v$3.vf $vfadr
mv -f $3.tfm $tfmadr
